# Copyright 2008, 2009 Ali Polatel <alip@exherbo.org>
# Copyright 2014 Calvin Walton <calvin.walton@kepstin.ca>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'pyopenssl-0.7.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

MY_PN=${PN/openssl/OpenSSL}
require pypi [ pn=${MY_PN} pnv=${MY_PN}-${PV} ]
require setup-py [ import=setuptools work=${MY_PN}-${PV} test=pytest ]

SUMMARY="Python wrapper module around the OpenSSL library"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    examples
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

# bind to 0.0.0.0
RESTRICT="test"

DEPENDENCIES="
    build:
        doc? (
            dev-python/Sphinx
            dev-python/sphinx_rtd_theme
        )
    build+run:
        dev-python/cryptography[>=2.8][python_abis:*(-)?]
        dev-python/six[>=1.5.2][python_abis:*(-)?]
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    test:
        dev-python/flaky[python_abis:*(-)?]
        dev-python/pretend[python_abis:*(-)?]
"

compile_one_multibuild() {
    setup-py_compile_one_multibuild

    if option doc ; then
        edo pushd doc
        edo mkdir -p .build/html .build/doctrees
        edo sphinx-build . html
        edo popd
    fi

}

install_one_multibuild() {
    setup-py_install_one_multibuild

    option doc && dodoc -r doc/html
    option examples && dodoc -r examples
}

