# Copyright 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools ]

SUMMARY="Extensions to the Python standard library unit testing framework"
DESCRIPTION="
Testtools is a set of extensions to the Python standard library's unit testing framework. These
extensions have been derived from many years of experience with unit testing in Python and come
from many different sources. Testtools supports Python versions all the way back to Python 2.6.
"
HOMEPAGE+=" http://launchpad.net/${PN}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/extras[>=1.0.0][python_abis:*(-)?]
        dev-python/linecache2[python_abis:*(-)?]
        dev-python/pbr[>=0.11][python_abis:*(-)?]
        dev-python/python-mimeparse[python_abis:*(-)?]
        dev-python/six[>=1.4.0][python_abis:*(-)?]
        dev-python/traceback2[python_abis:*(-)?]
        dev-python/unittest2[>=1.1.0][python_abis:*(-)?]
    suggestion:
        dev-python/fixtures[>=1.3.0][python_abis:*(-)?] [[ note = [ required for the test suite ] ]]
        dev-python/testresources[python_abis:*(-)?] [[ note = [ required for the test suite ] ]]
        dev-python/testscenarios[python_abis:*(-)?] [[ note = [ required for the test suite ] ]]
"

# https://bugs.launchpad.net/testtools/+bug/953371
PYTHON_BYTECOMPILE_EXCLUDES=( '_compat2x.py' )

test_one_multibuild() {
    # avoid a testtools <--> fixtures/testresources dependency loop
    if has_version dev-python/fixtures[python_abis:$(python_get_abi)] &&
        has_version dev-python/testresources[python_abis:$(python_get_abi)] &&
        has_version dev-python/testscenarios[python_abis:$(python_get_abi)]; then
            edo ${PYTHON} -m testtools.run testtools.tests.test_suite
    else
        ewarn "dev-python/fixtures[python_abis:$(python_get_abi)], dev-python/testresources[python_abis:$(python_get_abi)] and/or dev-python/testscenarios[python_abis:$(python_get_abi)] not yet installed, skipping tests."
    fi
}

