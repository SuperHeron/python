# Copyright 2018 Alexander Kapshuna <kapsh@kap.sh>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools ] utf8-locale

SUMMARY="Backport of standard pathlib module"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/six[python_abis:*(-)?]
        python_abis:2.7? ( dev-python/scandir[python_abis:2.7] )
    test:
        python_abis:2.7? ( dev-python/mock[python_abis:2.7] )
    post:
        dev-python/pytest[python_abis:*(-)?] [[ note = [ Runs tests ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PNV}-python2-test.patch )
PYTEST_PARAMS=( -v )

pkg_setup() {
    require_utf8_locale  # required for tests.
}

test_one_multibuild() {
    # Handle dependency loop pathlib2 <> pytest considering pytest update process.
    if has_version "dev-python/pytest[>=3.10.1][python_abis:$(python_get_abi)]"; then

        local workdir="${WORK}/${MULTIBUILD_CLASS}/${MULTIBUILD_TARGET}/${PNV}"
        local sockets="unix:${workdir}/@test_*_tmp/mysock"

        esandbox allow_net "${sockets}"
        # From setup-py.exlib
        PYTEST="py.test-$(python_get_abi)"
        PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTEST} "${PYTEST_PARAMS[@]}"
        esandbox disallow_net "${sockets}"

    else
        ewarn "Test dependencies are not installed yet"
    fi

}

